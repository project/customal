Currently in d7 the link module does not allow you to have a custom attrribute like aria-label
This module extends the features of the <a href="https://www.drupal.org/project/link">link module</a>
so that you can now add this attribute too.
